using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleScript : StateMachineBehaviour
{

    [SerializeField]
    private float timeUntilAnim;

    [SerializeField]
    private int numberAnimations;

    private float waitingTime;

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        waitingTime += Time.deltaTime;

        if (waitingTime > timeUntilAnim)
        {

            int boredAnimation = Random.Range(1, numberAnimations);


            animator.SetFloat("Idle", boredAnimation);

            waitingTime = 0;
        }
        
    }

}
