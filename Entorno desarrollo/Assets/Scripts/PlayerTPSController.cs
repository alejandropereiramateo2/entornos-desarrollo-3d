using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    private InputData input;
    private CharacterAnimBasedMovement  characterMovement;

    public UnityEvent onInteractionInput;

    public bool onInteractionZone
    {
        get; set;   
    }

    // Start is called before the first frame update
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        input.GetInput();

        if (onInteractionZone && input.jump)
        {
            onInteractionInput.Invoke();
        }

        characterMovement.MoveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);
    }
}
