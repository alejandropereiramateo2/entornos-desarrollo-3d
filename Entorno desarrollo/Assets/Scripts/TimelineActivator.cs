using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;


public class TimelineActivator : MonoBehaviour
{

    public PlayableDirector playableDirector;
    public string playerTag;
    public Transform interactionLocation;
    public bool autoActivate = false;

    public bool interact
    {
        get; set;
    }

    [Header("Activation Zone Events")]

    public UnityEvent OnPlayerEnter;
    public UnityEvent OnPlayerExit;

    [Header("Timeline Events")]

    public UnityEvent OnTimelineStart;
    public UnityEvent OnTimelineEnd;

    [Header("Resto De Variables")]

    public bool isPlaying;
    public bool playerInside;
    private Transform playerTransform;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals(playerTag))
        {
            playerInside = true;
            playerTransform = other.transform;
            OnPlayerEnter.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals(playerTag))
        {
            playerInside = false;
            playerTransform = null;
            OnPlayerExit.Invoke();
        }
    }

    private void PlayTimeline()
    {
        if (playerTransform && interactionLocation)
        {
            playerTransform.SetLocalPositionAndRotation(interactionLocation.position, interactionLocation.rotation);
        }

        if (autoActivate)
        {
            playerInside = false;
        }

        if (playableDirector)
        {
            playableDirector.Play();
        }

        isPlaying = true;
        interact = false;

        StartCoroutine(waitForTimelineToEnd());
    }

    private IEnumerator waitForTimelineToEnd()
    {
        OnTimelineStart.Invoke();

        float timeLineDuration = (float)playableDirector.duration;

        while (timeLineDuration > 0)
        {
            timeLineDuration -= Time.deltaTime;

            yield return null;
        }

        isPlaying = false;

        OnTimelineEnd.Invoke();

    }


    // Update is called once per frame
    void Update()
    {
        if (playerInside && !isPlaying)
        {
            if(interact || autoActivate)
            {
                PlayTimeline();
            }
        }
    }
}
